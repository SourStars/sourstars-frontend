import React from 'react';
import { makeStyles } from '@material-ui/core'
import { Container, Grid, BottomNavigation, BottomNavigationAction, Box, Paper } from '@material-ui/core';
import { Work, Restore, EmojiObjects } from '@material-ui/icons';
import { ProjectBar, ActionBar } from './SideBar';
import ConfigView from './ConfigView';
import ScanView from './ScanView';
import ReportView from './ReportView';
import DefectView from './DefectView';

const useStyles = makeStyles(theme => ({
  colCon: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    position: 'relative'
  },
  flexAuto: {
    flex: 'auto',
    overflow: 'auto'
  },
  alignCenter: {
    alignItems: 'center',
    paddingBlock: '30vh'
  },
  iconInText: {
    position: 'relative', 
    top: '5px'
  }
}));

export function Choice(props) {
  const { children, value, ...others } = props;

  return (
    <Box {...others}>
      { children.filter(child => value === child.props.index) }
    </Box>
  );
}

export function SideBar(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const {projects, actions, page, setPage, socket, ...others} = props;

  return (
    <Paper {...others}>
      <Choice className={classes.flexAuto} value={value}>
        <ProjectBar index={0} page={page} projects={projects} setPage={setPage} socket={socket}/>
        <ActionBar index={1} page={page} projects={projects} setPage={setPage} actions={actions} socket={socket}/>
      </Choice>
      <BottomNavigation
        value={value}
        onChange={(event, newValue) => setValue(newValue)}
        showLabels
      >
        <BottomNavigationAction label="Projects" icon={<Work />} />
        <BottomNavigationAction label="Actions" icon={<Restore />} /> 
      </BottomNavigation>
    </Paper>
  );
}

export function BlankView(props){
  const classes = useStyles();

  return (
    <Box {...props} className={`${props.className} ${classes.alignCenter}`}>
      <img src='images/SourStars.png' alt="SourStars Logo"></img>
      <span> <EmojiObjects className={classes.iconInText} />
        Create or open a project to continue ... 
      </span>
    </Box>
  );
}

export default function App(props) {
  const classes = useStyles();

  const {socket, ...others} = props;

  const [projects, setProjects] = React.useState({});
  const [actions, setActions] = React.useState({});
  
  const [page, setPage] = React.useState({name: 'blank'});

  const [logLine, setLogLine] = React.useState({});
  const [progress, setProgress] = React.useState({});

  React.useEffect(() => {
    socket.on('projects', data => setProjects(data));
    socket.on('actions', data => setActions(data));
    socket.on('error', data => console.log(`[ERROR:${data.event}] ${data.message}`));
    socket.on('progress', data => {
      setProgress(data);
      setActions(a => {
        a[data.action_id].progress = data.value;
        return a;
      });
    });
    socket.on('log_line', data => {
      setLogLine(data);
      setActions(a => {
        console.log(`[${data.action_id}] ${data.value}`);
        a[data.action_id].log += data.value;
        return a;
      });
    });
    socket.on('defect', data => {
      setPage({name: 'defect', ...data});
    });
    console.log('[init] socket init finished');
  // eslint-disable-next-line
  }, []);

  return (
    <Container disableGutters maxWidth='xl' {...others}>
      <Grid container>
        <Grid item xs={3}>
          <SideBar page={page} socket={socket} projects={projects} actions={actions} setPage={setPage} className={classes.colCon}/>
        </Grid>
        <Grid item xs={9}>
          {
            page.name === 'blank' ? <BlankView className={classes.colCon}/> :
            page.name === 'config' ? <ConfigView className={classes.colCon} 
              socket={socket} id={page.id} setPage={setPage} projects={projects}/> :
            page.name === 'scan' ? <ScanView className={classes.colCon}
              id={page.id} projects={projects} actions={actions} setPage={setPage} progress={progress} logLine={logLine}/> : 
            page.name === 'report' ? <ReportView className={classes.colCon} 
              id={page.id} socket={socket} projects={projects} actions={actions} setPage={setPage}/> :
            page.name === 'defect' ? <DefectView className={classes.colCon}
              id={page.action_id} setPage={setPage} projects={projects} actions={actions} defect_id={page.defect_id} info={page.info} code={page.code}/> : null
          }
        </Grid>
      </Grid>
    </Container>
  );
}
