import React from 'react';
import { makeStyles } from '@material-ui/core'
import { Box, Paper, Grid, Typography, Chip, Button } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Code, CalendarToday, Close, Assessment } from '@material-ui/icons';
import MonacoEditor from 'react-monaco-editor';

const useStyles = makeStyles(theme => ({
    marginClass: {
        background: 'lightPink'
    },
    contentClass: {
        background: 'lightBlue'
    }
}));

export default function DefectView(props) {
    const {id, defect_id, info, code, projects, actions, setPage, ...others} = props;
    const classes = useStyles();

    return (
        <Box {...others} style={{padding: '20px'}}>
            <Paper style={{height: '100%', padding: '10px', display: 'flex', flexDirection: 'column', justifyContent: 'space-around'}}>
            <Grid container>
                    <Grid item xs={9}>
                        <Typography> The {
                            (num => `${num}${
                                num % 10 === 1 ? 'st' : num % 10 === 2 ? 'nd' : num % 10 === 3 ? 'rd' : 'th'
                            }`)(defect_id + 1)
                        } defect in 
                            <Chip label={projects[actions[id].project_id].name} avatar={<Code />} style={{marginInline: '5px'}}
                                onClick={() => setPage({name: 'config', id: actions[id].project_id})}
                            />
                            created at 
                            <Chip label={new Date(actions[id].create_time * 1000).toLocaleString()} 
                                avatar={<CalendarToday />} style={{marginInline: '5px'}}
                            />
                            reported at 
                            <Chip label={new Date(actions[id].finish_time * 1000).toLocaleString()} 
                                avatar={<CalendarToday />} style={{marginInline: '5px'}}
                            />
                        </Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Box style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <Button startIcon={<Assessment/>} variant="contained" onClick={
                            () => setPage({name: 'report', id: id})
                        }> 
                            Return 
                        </Button>
                        <Button startIcon={<Close/>} variant="contained" onClick={() => setPage({name: 'blank'})} style={{marginLeft: '10px'}}> 
                            Close 
                        </Button>
                        </Box>
                    </Grid>
                </Grid>
                    <Alert style={{marginTop: '10px'}} severity="error"> {info.Type}: {info.Tip} </Alert>
                <MonacoEditor style={{marginTop: '16px'}} value={code} language='cpp' options={{readOnly: true}} editorDidMount={
                    (editor, monaco) => { 
                        editor.revealLineInCenter(info.Line);
                        editor.setPosition({lineNumber: info.Line, column: 1});
                        editor.deltaDecorations([], [
                            {
                                range: new monaco.Range(info.Line,1,info.Line,1),
                                options: {
                                    isWholeLine: true,
                                    minimap: true,
                                    className: classes.contentClass,
                                    marginClassName: classes.marginClass
                                }
                            }
                        ]);
                    }
                }/>
            </Paper>
        </Box>
    );
}
