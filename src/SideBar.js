import React from 'react';
import { makeStyles } from '@material-ui/core';
import { Box, List, ListItem, ListItemAvatar, Avatar, ListItemText, ListItemSecondaryAction, IconButton, Fab } from '@material-ui/core';
import { Code, Edit, Delete, PlayCircleFilled, Add, Search, HourglassFull, Replay, Visibility, Assessment, ReportProblem, FileCopy, SettingsRemote } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
    rightDown: {
        position: 'absolute',
        bottom: '10vh',
        right: '20px'
    },
    fullHeight: {
        height: '100%'
    }
}))

export function ProjectBar(props) {
    const classes = useStyles();
    const {projects, setPage, page, socket, ...others} = props;

    return (
        <Box {...others} className={classes.fullHeight}>
            <List>
            { Object.keys(projects).map((id) => (
                <ListItem key={id} button onClick={() => setPage({name: 'config', id: id})}>
                    <ListItemAvatar>
                        <Avatar>
                        {
                            projects[id].config.fetch.method === 'local' ? <FileCopy /> : 
                            projects[id].config.fetch.method === 'ssh' ? <SettingsRemote /> : <Code />
                        }
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={ projects[id].name } secondary={
                        projects[id].config.fetch.method === 'git' ? projects[id].config.fetch.url : projects[id].config.fetch.path
                    } />
                    <ListItemSecondaryAction>
                        <IconButton edge="end">
                            <PlayCircleFilled onClick={() => socket.emit('create_action', {project_id: id})}/>
                        </IconButton>
                        <IconButton edge="end" onClick={() => setPage({name: 'config', id: id})}>
                            <Edit />
                        </IconButton>
                        <IconButton edge="end" onClick={() => {
                            if(page.name === 'config' && page.id === id) {
                                setPage({name: 'blank'});
                            }
                            socket.emit('remove_project', {project_id: id});
                        }}>
                            <Delete />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            )) }
            </List>
            <Fab color="primary" className={classes.rightDown} onClick={() => setPage({name: 'config'})}>
                <Add />
            </Fab>
        </Box>
    );
}

export function ActionBar(props) {
    const classes = useStyles();
    const {projects, actions, page, setPage, socket, ...others} = props;

    return (
        <Box {...others} className={classes.fullHeight}>
            <List>
            { Object.keys(actions).map(id => (
                <ListItem key={id} button onClick={() => setPage({name: 'scan', id: id})}>
                    <ListItemAvatar>
                        <Avatar>
                            {
                                actions[id].status === 'prepared' ? <HourglassFull /> :
                                actions[id].status === 'scanning' ? <Search /> :
                                actions[id].status === 'aborted' ? <ReportProblem /> : <Assessment />
                            }
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={ projects[actions[id].project_id].name } secondary={
                        new Date(actions[id].create_time * 1000).toLocaleString()
                    } />
                    <ListItemSecondaryAction>
                        {
                            actions[id].status === 'aborted' || actions[id].status === 'finished' ? (
                                <IconButton edge="end" onClick={() => socket.emit('restart_action', {action_id: id})}>
                                    <Replay />
                                </IconButton>
                            ) : null
                        }
                        <IconButton edge="end" onClick={() => setPage({name: 'scan', id: id})}>
                            <Visibility />
                        </IconButton>
                        {
                            actions[id].status === 'finished' ? (
                                <IconButton edge="end" onClick={() => setPage({name: 'report', id: id})}>
                                    <Assessment />
                                </IconButton>
                            ) : null
                        }
                        {
                            actions[id].status === 'aborted' || actions[id].status === 'finished' ? (
                                <IconButton edge="end" onClick={() => {
                                    if(page.name === 'scan' && page.id === id) {
                                        setPage({name: 'blank'});
                                    }
                                    socket.emit('remove_action', {action_id: id});
                                }}>
                                    <Delete />
                                </IconButton>
                            ) : null
                        }
                    </ListItemSecondaryAction>
                </ListItem>
            )) }
            </List>
        </Box>
    );
}
