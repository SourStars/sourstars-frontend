import React from 'react';
import { Box, Paper, Chip, Typography, Grid, Button, LinearProgress } from '@material-ui/core';
import { Code, CalendarToday, Close, Assessment } from '@material-ui/icons';
import { Terminal } from 'xterm';
import { FitAddon } from 'xterm-addon-fit';
import { SearchAddon } from 'xterm-addon-search';
import 'xterm/css/xterm.css'

export class XTerm extends React.Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.getTerm = this.getTerm.bind(this);
        this.box = React.createRef();
    }

    render() {
        return <div {...this.props} ref={this.box}></div>
    }

    getTerm() {
        return this.term;
    }

    componentDidMount() {
        this.term = new Terminal({
            cursorBlink: true, 
            convertEol: true,
            fontFamily: 'consolas, courier-new, courier, monospace',
            fontSize: 15,
            ...this.props
        });
        this.fit = new FitAddon();
        this.search = new SearchAddon();
        this.term.loadAddon(this.fit);
        this.term.loadAddon(this.search);
        this.term.open(this.box.current);
        this.fit.fit();
    }
}

export default function ScanView(props) {
    const { id, projects, actions, setPage, logLine, progress, ...others } = props;

    const [selfProgress, setSelfProgress] = React.useState(0);

    const term = React.useRef();

    React.useEffect(() => {
        setSelfProgress(actions[id].progress);

        let t = term.current.getTerm();
        t.clear();
        t.write(actions[id].log);
    // eslint-disable-next-line
    }, [id, actions]);

    React.useEffect(() => {
        if (progress.action_id === id) {
            setSelfProgress(progress.value);
        }
    // eslint-disable-next-line
    }, [progress]);

    React.useEffect(() => {
        if (logLine.action_id === id) {
            let t = term.current.getTerm();
            t.write(logLine.value);
        }
    // eslint-disable-next-line
    }, [logLine]);

    return (
        <Box {...others} style={{padding: '20px'}}>
            <Paper style={{height: '100%', padding: '10px', display: 'flex', flexDirection: 'column', justifyContent: 'space-around'}}>
                <Grid container>
                    <Grid item xs={9}>
                        <Typography> An action for 
                            <Chip label={projects[actions[id].project_id].name} avatar={<Code />} style={{marginInline: '5px'}}
                                onClick={() => setPage({name: 'config', id: actions[id].project_id})}
                            />
                            created at 
                            <Chip label={new Date(actions[id].create_time * 1000).toLocaleString()} 
                                avatar={<CalendarToday />} style={{marginInline: '5px'}}
                            />
                            {
                                actions[id].status === 'prepared' ? 'is waiting in the queue' :
                                actions[id].status === 'scanning' ? 'is scanning' :
                                actions[id].status === 'aborted' ? ' was aborted at' :
                                actions[id].status === 'finished' ? 'was finished at' : null
                            }
                            {
                                actions[id].status === 'aborted' || actions[id].status === 'finished' ? (
                                    <Chip label={new Date(actions[id].finish_time * 1000).toLocaleString()} 
                                        avatar={<CalendarToday />} style={{marginInline: '5px'}}
                                    />
                                ) : null
                            }
                        </Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Box style={{display: 'flex', justifyContent: 'flex-end'}}>
                        {
                            actions[id].status === 'finished' ? (
                                <Button startIcon={<Assessment/>} variant="contained" color='primary' onClick={
                                    () => setPage({name: 'report', id: id})
                                }> 
                                    Report 
                                </Button>
                            ) : null
                        }
                        <Button startIcon={<Close/>} variant="contained" onClick={() => setPage({name: 'blank'})} style={{marginLeft: '10px'}}> 
                            Close 
                        </Button>
                        </Box>
                    </Grid>
                </Grid>
                <LinearProgress variant={ actions[id].status === 'prepared' ? 'indeterminate' : 'determinate'} value={selfProgress}/>
                <Box>
                    <XTerm style={{height: '70vh'}} ref={term}/>
                </Box>
            </Paper>
        </Box>
    );
}
