import React from 'react';
import { Box, Paper, TextField, Select, MenuItem, Typography, Grid, Switch, FormControlLabel, Chip, Dialog, Button, DialogContent, DialogActions, Input, InputLabel } from '@material-ui/core';
import { Add, Delete, Save, Close } from '@material-ui/icons'

export function FetchBlock(props) {
    const {args, setArgs, projects, id, defaultArgs, ...others} = props;

    const [method, setMethod] = React.useState(defaultArgs.config.fetch.method);
    const [useCache, setUseCache] = React.useState(defaultArgs.config.fetch.use_cache);

    const [url, setUrl] = React.useState(defaultArgs.config.fetch.url);
    const [branch, setBranch] = React.useState(defaultArgs.config.fetch.branch);

    const [path, setPath] = React.useState('');
    const [port, setPort] = React.useState(22);
    const [makeCopy, setMakeCopy] = React.useState(true);


    React.useEffect(() => {
        if(id) {
            setMethod(projects[id].config.fetch.method);
            setUseCache(projects[id].config.fetch.use_cache);
            if(projects[id].config.fetch.method === 'git') {
                setUrl(projects[id].config.fetch.url);
                setBranch(projects[id].config.fetch.branch);
            } else if(projects[id].config.fetch.method === 'ssh') {
                setPath(projects[id].config.fetch.path);
                setPort(projects[id].config.fetch.port);
            } else if(projects[id].config.fetch.method === 'local') {
                setPath(projects[id].config.fetch.path);
                setMakeCopy(projects[id].config.fetch.make_copy);
            }
        }
        else {
            setMethod(defaultArgs.config.fetch.method);
            setUseCache(defaultArgs.config.fetch.use_cache);
            setUrl(defaultArgs.config.fetch.url);
            setBranch(defaultArgs.config.fetch.branch);
        }
    // eslint-disable-next-line
    }, [id]);

    return (
        <Paper {...others} variant="outlined" style={{padding: '10px', display: 'flex', flexDirection: 'column'}}>
            <Box>
                <Typography component='span'> Fetch via </Typography>
                <Select value={method} 
                    onChange={(event) => {
                        const {value} = event.target;
                        setMethod(value);
                        setArgs(args => {
                            args.config.fetch.method = value;
                            
                            delete args.config.fetch.url;
                            delete args.config.fetch.branch;
                            delete args.config.fetch.path;
                            delete args.config.fetch.port;
                            delete args.config.fetch.make_copy;

                            if(value === 'git') {
                                args.config.fetch.url = url;
                                args.config.fetch.branch = branch;
                            } else if(value === 'ssh') {
                                args.config.fetch.port = port;
                                args.config.fetch.path = path;
                            } else if(value === 'local') {
                                args.config.fetch.path = path;
                                args.config.fetch.make_copy = makeCopy;
                            }

                            return args;
                        });
                    }}
                    style={{marginLeft: '5px', position: 'relative', top: '6px'}}
                    disabled={Boolean(id)}
                >
                    <MenuItem value='git'>Git</MenuItem>
                    <MenuItem value='ssh'>SSH</MenuItem>
                    <MenuItem value='local'>Local</MenuItem>
                </Select>
                <Typography component='span'  style={{marginLeft: '10px', marginRight: '5px'}}> , while </Typography>
                <FormControlLabel control={
                    <Switch color="primary" disabled={Boolean(id)} checked={useCache} onChange={
                        event => {
                            const {checked} = event.target;
                            setUseCache(checked);
                            setArgs(args => {
                                args.config.fetch.use_cache = checked;
                                return args;
                            })
                        }
                    }/>
                } label="using cache" style={{position: 'relative', top: '-2px'}}/>
            </Box>
            {
                method === 'git' ? (
                    <Grid container>
                        <Grid item xs={9}>
                            <TextField label="url" style={{width: '95%'}} disabled={Boolean(id)} value={url} onChange={
                                event => {
                                    const {value} = event.target;
                                    setUrl(value);
                                    setArgs(args => {
                                        args.config.fetch.url = value;
                                        return args;
                                    });
                                }
                            }/>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField label="branch" style={{width: '95%'}} disabled={Boolean(id)} value={branch} onChange={
                                event => {
                                    const {value} = event.target;
                                    setBranch(value);
                                    setArgs(args => {
                                        args.config.fetch.branch = value;
                                        return args;
                                    });
                                }
                            }/>
                        </Grid>
                    </Grid>
                ) : method === 'ssh' ? (
                    <Grid container>
                        <Grid item xs={9}>
                            <TextField label="path" style={{width: '95%'}} disabled={Boolean(id)} value={path} onChange={
                                event => {
                                    const {value} = event.target;
                                    setPath(value);
                                    setArgs(args => {
                                        args.config.fetch.path = value;
                                        return args;
                                    });
                                }
                            }/>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField label="port" style={{width: '95%'}} disabled={Boolean(id)} value={port} onChange={
                                event => {
                                    const {value} = event.target;
                                    setPort(value);
                                    setArgs(args => {
                                        args.config.fetch.port = value;
                                        return args;
                                    });
                                }
                            }/>
                        </Grid>
                    </Grid>
                ) : method === 'local' ? (
                    <Grid container>
                        <Grid item xs={9}>
                            <TextField label="path" style={{width: '95%'}} disabled={Boolean(id)} value={path} onChange={
                                event => {
                                    const {value} = event.target;
                                    setPath(value);
                                    setArgs(args => {
                                        args.config.fetch.path = value;
                                        return args;
                                    });
                                }
                            }/>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControlLabel control={
                                <Switch color="primary" disabled={Boolean(id)} checked={makeCopy}  onChange={
                                    event => {
                                        const {checked} = event.target;
                                        setMakeCopy(checked);
                                        setArgs(args => {
                                            args.config.fetch.make_copy = checked;
                                            return args;
                                        });
                                    }
                                }/>
                            } label="make a copy" style={{position: 'relative', top: '20px'}}/>
                        </Grid>
                    </Grid>
                ) : null
            }
        </Paper>
    );
}

export function BuildBlock(props) {
    const {args, setArgs, projects, id, defaultArgs, ...others} = props;

    const [method, setMethod] = React.useState('cmake');
    const [sourcePath, setSourcePath] = React.useState(defaultArgs.config.build.source_path);
    const [targets, setTargets] = React.useState([]);
    const [defs, setDefs] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [diagStatus, setDiagStatus] = React.useState('');
    const [diagInputStr, setDiagInputStr] = React.useState('');
    const [diagInputMap, setDiagInputMap] = React.useState({});

    React.useEffect(() => {
        if(id) {
            setSourcePath(projects[id].config.build.source_path);
            setTargets(projects[id].config.build.targets);
            setDefs(Object.keys(projects[id].config.build.variables).map(k => ({key: k, value: projects[id].config.build.variables[k]})));
        } else {
            setSourcePath(defaultArgs.config.build.source_path);
            setTargets(defaultArgs.config.build.targets);
            setDefs(Object.keys(defaultArgs.config.build.variables).map(k => ({key: k, value: defaultArgs.config.build.variables[k]})));
        }
    // eslint-disable-next-line
    }, [id]);

    React.useEffect(() => {
        setArgs(args => {
            args.config.build.variables = defs.reduce((obj, item) => Object.assign(obj, {[item.key]: item.value}), {});
            return args;
        });
    // eslint-disable-next-line
    }, [defs]);

    React.useEffect(() => {
        setArgs(args => {
            args.config.build.targets = targets;
            return args;
        });
    // eslint-disable-next-line
    }, [targets]);

    return (
        <Paper {...others} variant="outlined" style={{padding: '10px', display: 'flex', flexDirection: 'column'}}>
            <Box>
                <Typography component='span'> Build via </Typography>
                <Select value={method} 
                    onChange={(event) => setMethod(event.target.value)}
                    style={{marginLeft: '5px', position: 'relative', top: '6px'}}
                >
                    <MenuItem value='cmake'>CMake</MenuItem>
                </Select>
                <Typography component='span' style={{marginLeft: '10px', marginRight: '5px'}}> for </Typography>
                <TextField label="path (relative to root)" value={sourcePath} style={{position: 'relative', top: '-10px'}} onChange={
                    event => {
                        const {value} = event.target;
                        setSourcePath(value);
                        setArgs(args => {
                            args.config.build.source_path = value;
                            return args;
                        });
                    }
                }/>
            </Box>
            {
                method === 'cmake' ? (
                    <Box style={{display: 'flex', flexDirection: 'column', marginTop: '20px'}}>
                        <Box>
                            { targets.map(target => (
                                <Chip key={target} label={target} style={{margin: "2px"}} 
                                    onDelete={() => setTargets(targets => targets.filter(t => t !== target))}
                                />
                            )) }
                            <Chip label='add a target' color="primary" icon={<Add/>}
                                onClick={() => {
                                    setDiagStatus('target');
                                    setOpen(true);
                                }}
                            />
                        </Box>
                        <Box style={{marginTop: '10px'}}>
                            { defs.map(def => (
                                <Chip key={def.key} label={`${def.key}:${def.value}`} style={{margin: "2px"}} 
                                    onDelete={() => setDefs(defs => defs.filter(d => d.key !== def.key))}
                                />
                            )) }
                            <Chip label='add a definition' color="primary" icon={<Add/>}
                                onClick={() => {
                                    setDiagStatus('def');
                                    setOpen(true);
                                }}
                            />
                        </Box>
                    </Box>
                ) : null
            }
            <Dialog open={open} onClose={() => setOpen(false)}>
                <DialogContent>
                    {
                        diagStatus === 'target' ? (
                            <TextField
                                onChange={(event) => setDiagInputStr(event.target.value)} 
                                autoFocus label="target name"
                            />
                        ) : (
                            <Box style={{display: 'flex', flexDirection: 'column'}}>
                                <TextField
                                    onChange={(event) => {
                                        const { value } = event.target;
                                        setDiagInputMap(v => ({key: value, value: v.value}));
                                    }}
                                    autoFocus label="definition key"
                                />
                                <TextField
                                    onChange={(event) => {
                                        const { value } = event.target;
                                        setDiagInputMap(v => ({value: value, key: v.key}));
                                    }}
                                    label="definition value"
                                />
                            </Box>
                        )
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        const valid = diagStatus === 'target' ?
                            diagInputStr !== '' :
                            diagInputMap.key !== undefined && diagInputMap.value !== undefined;
                        if (valid) {
                            if (diagStatus === 'target') {
                                setTargets(targets => targets.concat(diagInputStr));
                            } else {
                                setDefs(defs => defs.concat(diagInputMap));
                            }
                            setDiagInputStr('');
                            setDiagInputMap({});
                            setOpen(false);
                        }
                    }} color="primary"
                    >
                        Ok
                    </Button>
                    <Button onClick={() => setOpen(false)} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </Paper>
    );
}

export function AnalyzeBlock(props) {
    const {args, setArgs, projects, id, defaultArgs, ...others} = props;

    const [ignoredDirs, setIgnoredDirs] = React.useState([]);
    const [open, setOpen] = React.useState(false);
    const [diagInput, setDiagInput] = React.useState('');
    
    let selected = [], setSelected = [];
    [selected[0], setSelected[0]] = React.useState([]);
    [selected[1], setSelected[1]] = React.useState([]);
    [selected[2], setSelected[2]] = React.useState([]);
    [selected[3], setSelected[3]] = React.useState([]);
    [selected[4], setSelected[4]] = React.useState([]);

    const AnalyzerCats = [
        {
            name: 'library usage', 
            value: ['Abseil', 'Android', 'Boost', 'MPI', 'OpenMP']
        }, {
            name: 'coding conventions', 
            value: ['CERT Secure', 'C++ Core Guidelines', 'Fuchsia', 'Google', 'High Integrity C++', 'Linux Kernel', 'LLVM', 'LLVM libc', 'MISRA C', 'MISRA C++', 'Objective-C', 'SourStars', 'Zircon']
        }, {
            name: 'security issue',
            value: ['Core', 'C++ specific', 'Dead code', 'Null pointer', 'Insecure API', 'Unix specific', 'OS X specific']
        }, {
            name: 'sourstars special',
            value: ['Dereference', 'Double Free & Use After Free', 'RAII Resource Leak', 'Overflow']
        }
    ];

    const AnalyzerSwitches = ['bugprone', 'modernize', 'performance', 'portability', 'readability'];

    const CheckerToNameMap = {
        'bugprone': 'bugprone',
        'modernize': 'modernize',
        'performance': 'performance',
        'portability': 'portability',
        'readability': 'readability',
        'abseil': 'Abseil',
        'android': 'Android',
        'boost': 'Boost',
        'mpi': 'MPI',
        'openmp': 'OpenMP',
        'cert': 'CERT Secure',
        'cppcoreguidelines': 'C++ Core Guidelines',
        'fuchsia': 'Fuchsia',
        'google': 'Google',
        'hicpp': 'High Integrity C++',
        'linuxkernel': 'Linux Kernel',
        'llvm': 'LLVM',
        'llvmlibc': 'LLVM libc',
        'misra-c': 'MISRA C',
        'misra-cpp': 'MISRA C++',
        'objc': 'Objective-C',
        'sourstars': 'SourStars',
        'zircon': 'Zircon',
        'clang-analyzer-core': 'Core',
        'clang-analyzer-cplusplus': 'C++ specific',
        'clang-analyzer-deadcode': 'Dead code',
        'clang-analyzer-nullability': 'Null pointer',
        'clang-analyzer-security': 'Insecure API',
        'clang-analyzer-unix': 'Unix specific',
        'clang-analyzer-osx': 'OS X specific',
        'clang-analyzer-sourstars.NewDereference': 'Dereference',
        'clang-analyzer-sourstars.DoubleFree': 'Double Free & Use After Free',
        'clang-analyzer-sourstars.CtorDtorLeak': 'RAII Resource Leak',
        'clang-analyzer-sourstars.OverFlow': 'Overflow',
    };

    const NameToCheckerMap = Object.keys(CheckerToNameMap).reduce((obj, item) => Object.assign(obj, {[CheckerToNameMap[item]]: item}), {});

    React.useEffect(() => {
        const setAllSelected = checkers => {
            for(let setEachSelected of setSelected) {
                setEachSelected([]);
            }

            for (let checker of checkers) {
                const name = CheckerToNameMap[checker];
                for (let i = 0; i < 4; ++i) {
                    if (AnalyzerCats[i].value.includes(name)) {
                        setSelected[i](s => s.concat([name]));
                    }
                }
                for(let n of AnalyzerSwitches) {
                    if(name === n) {
                        setSelected[4](s => s.concat([name]));
                    }
                }
            }
        };

        if (id) {
            setIgnoredDirs(projects[id].config.analyze.ignored_paths);
            setAllSelected(projects[id].config.analyze.checkers);
        } else {
            setIgnoredDirs(defaultArgs.config.analyze.ignored_paths);
            setAllSelected(defaultArgs.config.analyze.checkers);
        }
    // eslint-disable-next-line
    }, [id]);

    React.useEffect(() => {
        setArgs(args => {
            args.config.analyze.ignored_paths = ignoredDirs;
            return args;
        });
    // eslint-disable-next-line
    }, [ignoredDirs]);

    React.useEffect(() => {
        let allSelected = [];
        for(let s of selected) {
            allSelected = allSelected.concat(s);
        }

        setArgs(args => {
            args.config.analyze.checkers = allSelected.map(v => NameToCheckerMap[v]);
            return args;
        });
    // eslint-disable-next-line
    }, selected);

    return (
        <Paper {...others} variant="outlined" style={{padding: '10px', display: 'flex', flexDirection: 'column'}}>
            <Box>
                <Typography component='span' style={{marginRight: '10px', position: 'relative', top: '2px'}}> Analyze all project except </Typography>
                { 
                    ignoredDirs.map(dir => (
                        <Chip key={dir} label={dir} style={{margin: "2px"}} 
                            onDelete={() => setIgnoredDirs(dirs => dirs.filter(d => d !== dir))}
                        />
                    )) 
                }
                <Chip label='add a dir' color="primary" icon={<Add/>}
                    onClick={() => setOpen(true)}
                />
            </Box>
            <Box style={{marginTop: '10px', display: 'flex'}}>
                {
                    AnalyzerSwitches.map(s => (
                        <FormControlLabel control={<Switch color="primary" checked={selected[4].includes(s)} onChange={
                            event => {
                                if(event.target.checked) setSelected[4](sl => [...sl, s]);
                                else setSelected[4](sl => sl.filter(v => v !== s));
                            }
                        }/>} label={s} style={{flex: 'auto'}}/>
                    ))
                }
            </Box>
            <Grid container>
                { AnalyzerCats.map((cat, i) => (
                    <Grid item xs={6}  style={{marginTop: '20px'}}>
                        <InputLabel>{cat.name}</InputLabel>
                        <Select
                            multiple
                            value={selected[i]}
                            onChange={event => setSelected[i](event.target.value)}
                            input={<Input />}
                            renderValue={(selected) => (
                                <div style={{display: 'flex', flexWrap: 'wrap'}}>
                                {selected.map((value) => (
                                    <Chip key={value} label={value} style={{margin: '2px'}}/>
                                ))}
                                </div>
                            )}
                            style={{width: '95%'}}
                        >
                            {cat.value.map((v) => (
                                <MenuItem key={v} value={v}>
                                    {v}
                                </MenuItem>
                            ))}
                        </Select>
                    </Grid>
                ))}
            </Grid>
            <Dialog open={open} onClose={() => setOpen(false)}>
                <DialogContent>
                    <TextField
                        onChange={(event) => setDiagInput(event.target.value)} 
                        autoFocus label="ignored dir (relative to root)"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => {
                        if (diagInput !== '') {
                            setIgnoredDirs(dirs => dirs.concat(diagInput));
                            setDiagInput('');
                            setOpen(false);
                        }
                    }} color="primary"
                    >
                        Ok
                    </Button>
                    <Button onClick={() => setOpen(false)} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </Paper>
    );
}

export default function ConfigView(props) {
    const {projects, id, setPage, socket, ...others} = props;

    const defaultArgs = {name: '', config: {
        fetch: {method: 'git', url: '', branch: 'master', use_cache: true},
        build: {method: 'cmake', source_path: '.', targets: [], variables: {}},
        analyze: {checkers: [], ignored_paths: []}
    }};

    const [args, setArgs] = React.useState(defaultArgs);
    const [name, setName] = React.useState(defaultArgs.name);

    React.useEffect(() => {
        if(id) {
            setArgs(JSON.parse(JSON.stringify(projects[id])));
            setName(projects[id].name);
        } else {
            setArgs(defaultArgs);
            setName(defaultArgs.name);
        }
    // eslint-disable-next-line
    }, [id]);

    return (
        <Box {...others} style={{padding: '20px'}}>
            <Paper style={{height: '100%', padding: '10px', display: 'flex', flexDirection: 'column', justifyContent: 'space-around'}}>
                <Box style={{display: 'flex', justifyContent: 'flex-start'}}>
                    <TextField label="Project Name" variant="outlined" style={{width: "50%"}} value={name} onChange={
                        event => {
                            const {value} = event.target;
                            setName(value);
                            setArgs(args => {
                                args.name = value;
                                return args;
                            });
                        }
                    }/>
                    <Button variant="contained" color="primary" startIcon={<Save />} style={{marginLeft: '30%'}} onClick={
                        () => {
                            console.log(id, args);
                            if(id) {
                                socket.emit('edit_project', {project_id: id, new_name: args.name, new_config: args.config});
                            } else {
                                socket.emit('create_project', {name: args.name, config: args.config});
                            }
                            setPage({name: 'blank'});
                        }
                    }> Save </Button>
                    <Button variant="contained" startIcon={id ? <Close /> : <Delete />} style={{marginLeft: '10px'}} onClick={
                        () => setPage({name: 'blank'})
                    }> {id ? "Close" : "Drop"} </Button>
                </Box>
                <FetchBlock args={args} setArgs={setArgs} projects={projects} id={id} defaultArgs={defaultArgs}/>
                <BuildBlock args={args} setArgs={setArgs} projects={projects} id={id} defaultArgs={defaultArgs}/>
                <AnalyzeBlock  args={args} setArgs={setArgs} projects={projects} id={id} defaultArgs={defaultArgs}/>
            </Paper>
        </Box>
    );
}
