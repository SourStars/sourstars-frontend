import React from 'react';
import { Box, Paper, Grid, Typography, Chip, Button, TableContainer, Table, TableCell, TableBody, TableHead, TableRow } from '@material-ui/core';
import { Code, CalendarToday, Close, Visibility } from '@material-ui/icons';

export default function ReportView(props) {
    const {id, socket, projects, actions, setPage, ...others} = props;

    return (
        <Box {...others} style={{padding: '20px'}}>
            <Paper style={{height: '100%', padding: '10px', display: 'flex', flexDirection: 'column', justifyContent: 'space-around'}}>
            <Grid container>
                    <Grid item xs={9}>
                        <Typography> An action for 
                            <Chip label={projects[actions[id].project_id].name} avatar={<Code />} style={{marginInline: '5px'}}
                                onClick={() => setPage({name: 'config', id: actions[id].project_id})}
                            />
                            created at 
                            <Chip label={new Date(actions[id].create_time * 1000).toLocaleString()} 
                                avatar={<CalendarToday />} style={{marginInline: '5px'}}
                            />
                            reported at 
                            <Chip label={new Date(actions[id].finish_time * 1000).toLocaleString()} 
                                avatar={<CalendarToday />} style={{marginInline: '5px'}}
                            />
                        </Typography>
                    </Grid>
                    <Grid item xs={3}>
                        <Box style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <Button startIcon={<Visibility/>} variant="contained" onClick={
                            () => setPage({name: 'scan', id: id})
                        }> 
                            Log 
                        </Button>
                        <Button startIcon={<Close/>} variant="contained" onClick={() => setPage({name: 'blank'})} style={{marginLeft: '10px'}}> 
                            Close 
                        </Button>
                        </Box>
                    </Grid>
                </Grid>
                <TableContainer style={{height: '80vh'}} component={Paper} variant="outlined">
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="right">No.</TableCell>
                                <TableCell>Filename</TableCell>
                                <TableCell align="right">Line</TableCell>
                                <TableCell>Type</TableCell>
                                <TableCell>Tip</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {actions[id].report.Defects.map((row, index) => (
                                <TableRow key={index} hover onClick={ () => 
                                    socket.emit('query_defect', {action_id: id, defect_id: index}) 
                                }>
                                    <TableCell align="right">{index + 1}</TableCell>
                                    <TableCell>{row.Filename}</TableCell>
                                    <TableCell align="right">{row.Line}</TableCell>
                                    <TableCell>{row.Type}</TableCell>
                                    <TableCell>{row.Tip}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </Box>
    );
}
